package assaf.listview;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.Activity;
import android.app.ListActivity;
import android.graphics.Color;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends Activity{
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		ArrayList<String> list = new ArrayList<String>();
		list.add("Red");
		list.add("Blue");
		list.add("Green");
		list.add("Yellow");
		list.add("Orange");
		list.add("Purple");
		list.add("Grey");
		list.add("Pink");
		list.add("Black");
		list.add("White");
		list.add("Brown");

		
		ArrayAdapter<String>adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,list);
		ListView lv = (ListView)findViewById(R.id.list_view);
		lv.setAdapter(adapter);
		
		final TextView textView = (TextView)findViewById(R.id.text_lbl);
		
		OnItemClickListener listener = new OnItemClickListener(){  // Anonymous implementation of OnItemClickListener interface
			public void onItemClick(AdapterView parent, View v, int position, long id){
				switch(position){
				case 0:	
					textView.setBackgroundColor(Color.parseColor("#FF0000"));
					break;
				case 1:
					textView.setBackgroundColor(Color.parseColor("#0000FF"));
					break;
				case 2:
					textView.setBackgroundColor(Color.parseColor("#008000"));
					break;
				case 3:
					textView.setBackgroundColor(Color.parseColor("#FFFF00"));
					break;
				case 4:
					textView.setBackgroundColor(Color.parseColor("#FFA500"));
					break;
				case 5:
					textView.setBackgroundColor(Color.parseColor("#800080"));
					break;
				case 6:
					textView.setBackgroundColor(Color.parseColor("#808080"));
					break;
				case 7:
					textView.setBackgroundColor(Color.parseColor("#FF00FF"));
					break;
				case 8:
					textView.setBackgroundColor(Color.parseColor("#000000"));
					break;
				case 9:
					textView.setBackgroundColor(Color.parseColor("#FFFFFF"));
					break;
				case 10:
					textView.setBackgroundColor(Color.parseColor("#A52A2A"));
					break;
				}
			}
		};
		
		lv.setOnItemClickListener(listener);

	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}


}
